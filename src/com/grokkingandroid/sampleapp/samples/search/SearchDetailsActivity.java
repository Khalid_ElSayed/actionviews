/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.grokkingandroid.sampleapp.samples.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.R;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.ActionViewActivity;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.BaseActivity;

public class SearchDetailsActivity extends BaseActivity {

   public static final String KEY_WORD = "word";

   public static final String[] ENGLISH_WORDS = {"one", "two", "three"};
   private static final String[] GERMAN_TRANSLATIONS = {"eins", "zwei", "drei"};
   
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_detail);
		Bundle bundle = getIntent().getExtras();
		// very inefficient, but well, that's not the point of this demo...
		String englishWord =  bundle.getString(KEY_WORD);
      ((TextView)findViewById(R.id.english)).setText(englishWord);
		for (int i = 0; i < ENGLISH_WORDS.length; i++) {
		   if (ENGLISH_WORDS[i].equals(englishWord)) {
		      ((TextView)findViewById(R.id.german)).setText(GERMAN_TRANSLATIONS[i]);
		      break;
		   }
		}
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         case android.R.id.home:
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this,
                  new Intent(this, ActionViewActivity.class));
            return true;
      }
      return super.onOptionsItemSelected(item);
   }	

}
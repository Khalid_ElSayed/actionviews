/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.grokkingandroid.sampleapp.samples.actionbar.actionviews;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.R;


public class BaseActivity extends SherlockFragmentActivity {
   
   @Override
   protected void onCreate(Bundle icicle) {
      super.onCreate(icicle);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getSupportMenuInflater().inflate(R.menu.activity_about_item, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      if (item.getItemId() == R.id.about) {
         showDialog();
         return true;
      }
      return super.onOptionsItemSelected(item);
   }

   private void showDialog() {
      DialogFragment newFragment = AboutFragment.newInstance();
      newFragment.show(getSupportFragmentManager(), "dialog");
   }
}

/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.grokkingandroid.sampleapp.samples.actionbar.actionviews;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.R;


public class ActionViewActivity extends BaseActivity {

   private String[] mSpinnerArray;
   private int mCurrSpinnerPos= 0;
   private static final String KEY_SPINNER_POS = "keySpinnerPos"; 
   
   @Override
   public void onCreate(Bundle icicle) {
      super.onCreate(icicle);
      getSupportActionBar().setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
      setContentView(R.layout.activity_fragment_container);

      int resId = R.array.spinner_without_searchview;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
         resId = R.array.spinner_with_searchview;
      }
      mSpinnerArray = getResources().getStringArray(resId);      
      ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getSupportActionBar().getThemedContext(), resId, R.layout.sherlock_spinner_item);
      spinnerAdapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
      getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
      getSupportActionBar().setListNavigationCallbacks(spinnerAdapter,
            new OnNavigationListener() {
               @Override
               public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                  if (itemPosition == mCurrSpinnerPos) {
                     return true;
                  }
                  int pos = itemPosition;
                  if (mSpinnerArray.length == 3) {
                     pos++;
                  }
                  Fragment f = null;
                  switch (pos) {
                  case 0:
                     f = SearchViewFragment.newInstance();
                     break;
                  case 1:
                     f = PlaceholderActionViewFragment.newInstance();
                     break;
                  case 2:
                     f = PreExpandedActionViewFragment.newInstance();
                     break;
                  case 3:
                     f = ExpandableActionViewFragment.newInstance();
                     break;
                  }
                  getSupportFragmentManager()
                     .beginTransaction()
                     .replace(R.id.demo_fragment_container, f)
                     .commit();
                  mCurrSpinnerPos = itemPosition;
                  return true;
               }
            });

      if (icicle != null) {
         int spinnerPos = icicle.getInt(KEY_SPINNER_POS, 0);
         mCurrSpinnerPos = spinnerPos;
         getSupportActionBar().setSelectedNavigationItem(spinnerPos);
         return;
      }

      Fragment fragment = null;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
         // SearchView only from API level 8 onwards
         fragment = SearchViewFragment.newInstance();
      } else {
         fragment = PlaceholderActionViewFragment.newInstance();
      }
      getSupportFragmentManager()
            .beginTransaction()
            .add(R.id.demo_fragment_container, fragment)
            .commit();
   }

   @Override
   protected void onSaveInstanceState(Bundle outState) {
      super.onSaveInstanceState(outState);
      outState.putInt(KEY_SPINNER_POS, mCurrSpinnerPos);
   }

}

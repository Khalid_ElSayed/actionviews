/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.grokkingandroid.sampleapp.samples.actionbar.actionviews;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.grokkingandroid.sampleapp.samples.actionbar.actionviews.R;


public class PreExpandedActionViewFragment extends DemoBaseFragment {

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_fragment_preexpanded, menu);
	}

	public static PreExpandedActionViewFragment newInstance() {
		return new PreExpandedActionViewFragment();
	}

   @Override
   public String getDescriptionText() {
      return getResources().getString(R.string.preexpanded_desc);
   }

}
